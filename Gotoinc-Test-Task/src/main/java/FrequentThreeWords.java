import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class FrequentThreeWords {

    public static String[] threeWordsFunction(String text) {
        if (text == null || text.isBlank()) {
            return new String[0];
        }
        String removedPunctuationText = text.replaceAll("[!?,.\\-:;()]+", "");
        String[] notVerifiedWords = removedPunctuationText.split(" ");
        HashMap<String, Integer> words = new HashMap<>();
        for (int i = 0; i < notVerifiedWords.length; i++) {
            String currentWord = notVerifiedWords[i];
            if (isWord(currentWord) && !currentWord.isBlank()) {
                currentWord = currentWord.toLowerCase();
                if (words.containsKey(currentWord)) {
                    int j = words.get(currentWord);
                    j++;
                    words.put(currentWord,j);
                } else {
                    words.put(currentWord, 1);
                }
            }
        }
        if (words.size() < 3) return new String[0];

        Map<String, Integer> sortedMap = words.entrySet().stream()
                .sorted(Map.Entry.<String,Integer>comparingByValue().reversed())
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                                (e1,e2) -> e1, LinkedHashMap::new));

        String[] result = new String[3];
        Iterator<String> iterator = sortedMap.keySet().iterator();
        for (int i = 0; i < 3; i++) {
            result[i] = iterator.next();
        }
        return result;
    }

    private static boolean isWord(String word) {
        for (int i = 0; i < word.length(); i++) {
            if (!(word.charAt(i) >= 'a' && word.charAt(i)<= 'z' || word.charAt(i) >= 'A' && word.charAt(i) <= 'Z'|| word.charAt(i) =='\'')) {
                return false;
            }
        }
        return true;
    }
}
