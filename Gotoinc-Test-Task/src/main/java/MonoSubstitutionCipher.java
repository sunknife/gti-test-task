public class MonoSubstitutionCipher {

    public static String encrypt(String text, int n) {
        if (text == null || text.isEmpty() || n <= 0) {
            return text;
        }
        String result = text;
        for (int i = 0; i < n; i++){
            result = cypher(result);
        }
        return result;
    }

    private static String cypher(String text) {
        StringBuilder result = new StringBuilder();
        StringBuilder oddNumbers = new StringBuilder();
        for (int i = 0; i < text.length(); i++){
            if (i % 2 == 0) {
                oddNumbers.append(text.charAt(i));
            } else {
                result.append(text.charAt(i));
            }
        }
        result.append(oddNumbers);
        return result.toString();
    }

    public static String decrypt(String encrypted_text, int n) {
        if (encrypted_text == null || encrypted_text.isEmpty() || n <= 0) {
            return encrypted_text;
        }
        String result = encrypted_text;
        for (int i = 0; i < n; i++) {
            result = decypher(result);
        }
        return result;
    }

    private static String decypher(String encrypted_text) {
        StringBuilder result = new StringBuilder();
        for (int i = encrypted_text.length() / 2; i < encrypted_text.length(); i++) {
            int j = i - encrypted_text.length() /  2;
            result.append(encrypted_text.charAt(i));
            if (j < encrypted_text.length() /  2) {
                result.append(encrypted_text.charAt(j));
            }
        }
        return result.toString();
    }
}
